package alph1;
import java.util.regex.*;
import java.util.*;
import java.io.*;

public class Alph1 {
    public static <K extends Comparable<K>, V> int byFirst(Map.Entry<K, V> a, Map.Entry<K, V> b) {
        return a.getKey().compareTo(b.getKey());
    }
    public static <K, V extends Comparable<V>> int bySecond(Map.Entry<K, V> a, Map.Entry<K, V> b) {
        return a.getValue().compareTo(b.getValue());
    }
    public static void printflush(PrintWriter w, String fmt, Object ...args) {
        w.printf(fmt, args);
        w.flush();
    }
    public static String capitalize(String word) {
        if(word.equals("")) {
            return word;
        } else {
            return word.substring(0, 1).toUpperCase() + word.substring(1);
        }
    }
    private final static Pattern WORDSPLIT = Pattern.compile("\\W+");
    public static void buildTable(Map<String, Long> table, BufferedReader r) throws IOException {
        String line;
        while((line = r.readLine()) != null) {
            for(var word : WORDSPLIT.split(line)) {
                word = capitalize(word);
                if(word != "") {
                    table.put(word, table.getOrDefault(word, 0L) + 1L);
                }
            }
        }
    }
    public static <K, V> void output(PrintWriter w, Iterable<Map.Entry<K, V>> pairs) {
        for(var pair : pairs) {
            printflush(w, "%24s: %10s\n", pair.getKey(), pair.getValue());
        }
    }
    public static <K, V> void report(PrintWriter w, Map<K, V> table, Comparator<Map.Entry<K, V>> o) throws IOException {
        var pairs = new ArrayList<Map.Entry<K, V>>();
        for(var pair : table.entrySet()) {
            pairs.add(pair);
        }
        Collections.sort(pairs, o);
        output(w, pairs);
    }
}
