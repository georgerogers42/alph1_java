package alph1;
import java.util.*;
import java.io.*;
import org.apache.commons.cli.*;

public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Comparator<Map.Entry<String, Long>> cmp = (a, b) -> Alph1.bySecond(a, b);
        CommandLineParser parser = new DefaultParser();
        Options o = new Options();
        o.addOption(Option.builder().longOpt("a").build());
        try {
            CommandLine cl = parser.parse(o, args);
            if(cl.hasOption("a")) {
                cmp = (a, b) -> Alph1.byFirst(a, b);
            }
        } catch (ParseException e) {
            var hp = new HelpFormatter();
            hp.printHelp("alph1", "", o, "", true);
            return;
        }
        PrintWriter stdout = new PrintWriter(new OutputStreamWriter(System.out));
        for(int i = 1; i <= 100; i++) {
            Alph1.printflush(stdout, "# %22s: %10d\n", "Begin Iteration", i);
            var table = new HashMap<String, Long>();
            Reader f = new FileReader("TEXT-PCE-127.txt");
            try {
                BufferedReader r = new BufferedReader(f);
                Alph1.buildTable(table, r);
            } finally {
                f.close();
            }
            Alph1.report(stdout, table, cmp);
            Alph1.printflush(stdout, "# %22s: %10d\n", "End Iteration", i);
        }
    }
}
